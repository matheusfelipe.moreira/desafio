# Docker Pets  [![pipeline status](https://gitlab.com/matheusfelipe.moreira/desafio/badges/master/pipeline.svg)](https://gitlab.com/matheusfelipe.moreira/desafio/commits/master) [![coverage report](https://gitlab.com/matheusfelipe.moreira/desafio/badges/master/coverage.svg)](https://gitlab.com/matheusfelipe.moreira/desafio/commits/master)
Docker Pets is a simple application that's useful for testing out features of Docker Enterprise Edition.

If you are interested in a guide on how to demo Docker Pets on the Universal Control Plane then check out [this tutorial](https://github.com/docker/dcus-hol-2017/tree/master/docker-enterprise).


## Versioning

- `1.0` is the primary version that should be used for demos. `latest` is also tagged with `1.0`

## Application Architecture

`docker-pets` can be run as a stateless single-container application or as a multi-container stateful application. The following are the two images used to deploy `docker-pets`:

- `chrch/docker-pets` is a front-end Python Flask container that serves up random images of housepets, depending on the given configuration
- `consul` is a back-end KV store that stores the number of visits that the `web` services recieve. It's configured to bootstrap itself with 3 replicas so that we have fault tolerant persistence.

## Building Pets from Scratch
Pets is hosted on the Docker Hub at `chrch/docker-pets:latest` but you can also build it locally with the following steps

```
$ git clone https://gitlab.com/matheusfelipe.moreira/docker-pets.git
$ cd docker-pets
$ docker-compose up
```

## Pets configuration parameters
The `web` container has several configuration parameters as environment variables:


- **`DB`**: Tells `web` where to find `db`. Service name or `<ip>:<port>`.
- **`DEBUG`**: Puts `web` containers in to debug mode. When mounting a volume for code, they will restart automatically when they detect a change in the code. Defaults to off, set to `True` to turn on.
- **`ADMIN_PASSWORD_FILE`**: Turns secrets on. If set, will password protect the Admin Console of `web`. Set to the full location of the Swarm secret (`/run/secrets/< X >`)

## Exposed Services
- Client Web Access - (dev port `5000`, prod URL `pets.dckr.org`)
	- `/` shows the selected Pet
	- `/vote` displays the vote selection
	- `/health` displays the application health of the given container
	- `/kill` toggles the health off for one of the web servers
- Admin Console - (dev port `7000`, prod URL `admin.pets.dckr.org`)
	- `/` displays voting results, redirects to `/login` if secrets are configured
	- `/login` requests login password
- Consul Backend - (dev port `8500`, prod ephemeral port)
	- `/ui` displays Consul server UI

## Voting Option Configuration

- **`OPTION_A`**: Defaults to 'Cats'. Pictures located in `/docker-petspets/web/static/option_a`
- **`OPTION_B`**: Defaults to 'Dogs'. Pictures located in `/docker-pets/web/static/option_b`
- **`OPTION_C`**: Defaults to 'Whales'. Pictures located in `/docker-pets/web/static/option_c`

